

cave_y: defw 0
cave_ySpeed: defw 0
CAVE_HEIGHT: equ 32

CAVE_BORDER: equ 4
CAVE_MINY: equ (CAVE_BORDER) ^ 0x80
CAVE_MAXY: equ (SCREEN_HEIGHT-CAVE_BORDER-CAVE_HEIGHT) ^ 0x80

cave_frameCount: defw 0


; Should be called to enter the cave scene.
;
; (simple)
;
; Parameters:
;       NONE.
; Modifies:
;       a, bc, de, hl
; Returns:
;       NONE
;
cave_start:
    ; Fill the screen with zeroes
    ld e, 0x0
    call generic_fillScreenNicely
    
    ; Load cave_y with the starting position.
    ld hl, ((SCREEN_HEIGHT - CAVE_HEIGHT)/2) << 4
    ld (cave_y), hl
    
    ; Load cave_ySpeed with the initial speed: Zero.
    ld hl, 0
    ld (cave_ySpeed), hl
    
    ; Load c and d with the top and bottom position of the cave.
    ld c, (SCREEN_HEIGHT - CAVE_HEIGHT)/2
    ld d, (SCREEN_HEIGHT + CAVE_HEIGHT)/2
    
    ; Load d with the value 128, for convenience.
    ld b, 128
    
    ; XOR c and d with 128 so we can do unsigned comparison.
    ld a, c
    xor b
    ld c, a
    ld a, d
    xor b
    ld d, a
    
    ; Here, e is the line within the screen we are using.
    ld e, 0
    cave_startLoop:
        
        ld a, e
        cp c       ; If our y position is less than c, draw a line.
        jp p, cave_startDrawLine
        
        xor b
        cp d       ; Or, if our y position is greater or equal to d, draw a line.
        jp p, cave_startDrawLine
        
        jp cave_startDrawEmpty  ; Otherwise, don't draw a line.
        
        cave_startDrawLine:
            call queue1_addLine
        cave_startDrawEmpty:
        
        
        inc e
        ld a, e  ; Loop until e reaches 192
        sub 192
    jp nz, cave_startLoop
    
    ; Now we have finished setting up the cave, we must do other stuff.
    ; So do the initialisation for the player.
    call player_init
    ; Next we run through 8 frames, to ensure no speed steps run in the first 8 frames:
    
    ld a, 8
    cave_start8FrameLoop:
        push af
            call queue0_step
            call queue1_step
            call generic_waitForNextFrame
        pop af
        dec a
    jp nz, cave_start8FrameLoop
ret




; Should be called each step of the cave scene
;
; (simple)
;
; Parameters:
;       NONE.
; Modifies:
;       hl, de
; Returns:
;       NONE
;
cave_step:
    
    ; Load the frame count, increment it and store it back.
    ld a, (cave_frameCount)
    inc a
    ld (cave_frameCount), a
    
    ; get if the least significant three bits of the frame count are zero, i.e: One in every 8 frames:
    and 7
    jp nz, cave_stepNoYSpeedStep
        call cave_ySpeedStep
    cave_stepNoYSpeedStep:
    
    ; Ensure the cave does not go off the screen.
    call cave_yBoundsCheck
    ; Every frame, this function adds the yspeed onto the cave's y position.
    ; This method returns the single-byte values of old and new y position.
    call cave_applyVSpeed
    ; From this, we now call the method to update the drawing of the cave, based on the difference in y position.
    call cave_differenceMove
    ; Finally we call the world system's queue draw-updating methods.
    call queue0_step
    call queue1_step
ret



; Should be called after we exit from the cave scene.
;
; (simple)
;
; Parameters:
;       NONE.
; Modifies:
;       hl
; Returns:
;       NONE
;
cave_exit:
    ; Reset the cave_frameCount
    ld hl, cave_frameCount
    ld (hl), 0
    
    ; Clear both world queues
    call queue0_clear
    call queue1_clear
ret








; Moves the cave vertically.
;
; (simple)
;
; Parameters:
;       e: The old vertical position of the cave.
;       l: The new vertical position of the cave.
; Modifies:
;       a, e
; Returns:
;       e: The one-byte real value for the cave y position.
;
cave_differenceMove:
    ; e=old, l=new
    ld h, 0x80
    
    ld a, l
    xor h
    ld l, a
    ; e=old, l=new xor 0x80, h=0x80
    ld a, e
    xor h
    cp l
    ; e=old, l=new xor 0x80, a=old xor 0x80, h=0x80
    
    ; Ensure nothing happens if it is requested that we do not move.
    ret z
    
    jp p, cave_differenceMoveUp
        ; We have moved down, move down
        call cave_moveDown
        ret
        
    cave_differenceMoveUp:
        ; If we have moved up, move up.
        call cave_moveUp
ret




; Called by cave_differenceMove
cave_moveUp:
    ; e=old, l=new xor 0x80, a=old xor 0x80, h=0x80
    ld a, l
    xor h
    ld l, a
    ; e=old, l=new, a=new
    ex de, hl
    ld d, l
    ; e=new, d=old, a=new
    
    cave_moveUpLoop1:
        call queue0_addLine   ; This loop body could be made more efficient
        ld a, e               ; by using another register - check if this is useful.
        add CAVE_HEIGHT
        ld e, a
        call queue1_addLine
        ld a, e
        sub CAVE_HEIGHT
        inc a
        ld e, a
        
        cp d
    jp nz, cave_moveUpLoop1
ret



cave_moveDown:
    ; e=old, l=new xor 0x80, a=old xor 0x80, h=0x80
    ld a, l
    xor h
    ld l, a
    ; e=old, l=new, a=new
    ld a, e
    ld d, l
    ; e=old, d=new, a=old
    
    cave_moveDownLoop1:
        call queue1_addLine   ; This loop body could be made more efficient
        ld a, e               ; by using another register - check if this is useful.
        add CAVE_HEIGHT
        ld e, a
        call queue0_addLine
        ld a, e
        sub CAVE_HEIGHT
        inc a
        ld e, a
        
        cp d
    jp nz, cave_moveDownLoop1
ret










; Applies the vertical speed to the cave's position, and loads the old and new
; one-byte y position values.
;
; (simple)
;
; Parameters:
;       NONE
; Modifies:
;       a, hl, de, c
; Returns:
;       e: The cave's old vertical 0position.
;       l: The cave's new vertical position.
;
cave_applyVSpeed:
    ld hl, (cave_y)
    call generic_loadTwoByte
    ld c, e
    ld de, (cave_ySpeed)
    add hl, de
    ld (cave_y), hl
    call generic_loadTwoByte
    ld l, e
    ld e, c
ret



; Randomly updates the value of the yspeed of the cave. This should be called once per 8 frames
; at most, and must not be called within the first 8 frames.
;
; (simple)
;
; Parameters:
;       NONE.
; Modifies:
;       a, hl, de, bc
; Returns:
;       NONE.
;
cave_ySpeedStep:
    ld hl, (cave_ySpeed)
    
    ld a, r
    and 7
    jp nz, cave_ySpeedStepNRZ
        ld a, 4
    cave_ySpeedStepNRZ:
    sub 4
    
    jp m, cave_ySpeedStepNeg
        ld e, a
        ld d, 0
    jp cave_ySpeedStepN
    cave_ySpeedStepNeg:
        ld e, a
        ld d, -1
    cave_ySpeedStepN:
    
    
    
    
    ; Now we have the value to add to the speed in de.
    ; Here is the code to safeguard against reversing the direction in one step.
    xor a
    cp h
    jp nz, cave_ySpeedStepCheckedIncrement
    cp l
    jp nz, cave_ySpeedStepCheckedIncrement
        ; If the original speed was already zero, this does not matter, so add without checks.
        add hl, de
        ld (cave_ySpeed), hl
    ret
    cave_ySpeedStepCheckedIncrement:
        ; If the original speed is not zero, then back-up the most significant byte of the original speed.
        ld c, h
        ; Perform the addition.
        add hl, de
        
        ; If the new speed and old speed have different signs:
        ld a, c
        xor h
        and 0x80
        jp z, cave_ySpeedStepEnd
            ; Then set the new speed to zero, to avoid changing direction in one step.
            ld hl, 0
        cave_ySpeedStepEnd:
        
        ld (cave_ySpeed), hl
ret







cave_yBoundsCheck:
    ld hl, (cave_y)
    call generic_loadTwoByte
    ld a, e
    xor 0x80
    
    ; If cave y is less than 128
    jp p, cave_yBoundsCheckBottom
        
        cp CAVE_MINY
        ret p
        ; If cave y is less than min y
        
        ; Get cave y speed
        ld hl, (cave_ySpeed)
        ld a, h
        or a
        ; Do nothing if y speed is positive: Moving away from top.
        ret p
        ; Set y speed to zero if the cave is moving upwards
        ld hl, 0
        ld (cave_ySpeed), hl
        
        ; Set the frame counter to 1, so that no speed steps happen for at least 8 frames.
        ld a, 1
        ld (cave_frameCount), a
        
        ret
    
    ; If cave y is greater than 128
    cave_yBoundsCheckBottom:
        
        cp CAVE_MAXY
        ret m
        ; If cave y is greater than max y
        
        ; Get cave y speed
        ld hl, (cave_ySpeed)
        
        ; Return if hl is less than or equal to zero - BETTER WAY TO CHECK THIS NEEDED
        ld a, h
        or a
        ret m
        jp nz, cave_yBoundsCheckBottomRun
        ld a, l
        or a
        ret z
        cave_yBoundsCheckBottomRun:
        
        ; Set y speed to zero if the cave is moving downwards
        ld hl, 0
        ld (cave_ySpeed), hl
        
        ; Set the frame counter to 1, so that no speed steps happen for at least 8 frames.
        ld a, 1
        ld (cave_frameCount), a
        
        ret
ret
