

; Player size constants
PLAYER_HEIGHT: equ 9

PLAYER_ACCELERATION: equ 6
PLAYER_INITIAL_YSPEED: equ 128


player_y: defw 0
player_ySpeed: defw 0



; Draws the initial position of the player, and resets all variables for the player.
; This should be called by cave_start.
;
; (simple)
;
; Parameters:
;       NONE.
; Modifies:
;       a, hl
; Returns:
;       NONE
;
player_init:
    
    ld hl, 0
    ld (player_ySpeed), hl

    ld hl, ((SCREEN_HEIGHT - PLAYER_HEIGHT)/2) << 8
    ld (player_y), hl
    
    ld e, h
    call player_drawPlayer
ret



; Runs the start step for the player.
;
; (simple)
;
; Parameters:
;       NONE.
; Modifies:
;       a, bc, de, hl
; Returns:
;       NONE
;
player_startStep:
    
    ; c will store zero at the end if neither w nor s has been pressed.
    ld c, 0
    
    
    ; Check for key press of w key.
    
    ; Load a from port fbfe
    ld a, 0xfb   ; Address byte:       Most significant byte of port number
    in a, (0xfe) ; Input port byte:   Least significant byte of port number
    
    ; Get bit 2 - the w key bit.
    and 2
    jp nz, player_startStepNoMoveUp
        ld hl, (player_ySpeed)
        ld a, h
        or a
        jp p, player_startStepwRes
            ; If we are already moving up, accelerate the player upwards.
            ld de, 0 - PLAYER_ACCELERATION
            add hl, de
        jp player_startStepwEnd
        player_startStepwRes:
            ; If we are moving downwards, or our yspeed is zero, then set the speed to -PLAYER_INITIAL_YSPEED
            ld hl, 0 - PLAYER_INITIAL_YSPEED
        player_startStepwEnd:
        ld (player_ySpeed), hl
        ld c, 1
    player_startStepNoMoveUp:
    
    
    ; Check for key press of s key.
    
    ; Load a from port fdfe
    ld a, 0xfd   ; Address byte:       Most significant byte of port number
    in a, (0xfe) ; Input port byte:   Least significant byte of port number
    
    ; Get bit 2 - the s key bit.
    and 2
    jp nz, player_startStepNoMoveDown
        ld hl, (player_ySpeed)
        dec hl
        ld a, h
        or a
        jp m, player_startStepsRes
            ; If we are moving downwards already, accelerate the player downwards.
            inc hl
            ld de, PLAYER_ACCELERATION
            add hl, de
        jp player_startStepsEnd
        player_startStepsRes:
            ; If we are moving upwards, or our yspeed is zero, then set the speed to PLAYER_INITIAL_YSPEED
            ld hl, PLAYER_INITIAL_YSPEED
        player_startStepsEnd:
        ld (player_ySpeed), hl
        ld c, 1
    player_startStepNoMoveDown:
    
    ; If no keys are pressed:
    ld a, c
    or a
    jp nz, player_startStepNSR
        ; Cancel the vertical speed.
        ld hl, 0
        ld (player_ySpeed), hl
    player_startStepNSR:
    
    
    call player_yPosStep
    
ret



; Runs the end step for the player.
;
; (simple)
;
; Parameters:
;       NONE.
; Modifies:
;       a, de, hl
; Returns:
;       a: A zero value if there is no player collision, a non-zero value if there is a player collision.
;          Note: Flags for the value are automatically set.
;
player_checkCollision:
    ; Store the value that should be at the top and bottom of the spaceship
    ; into d for convenience.
    ld d, 00111111b
    
    ; Load into hl the screen memory location of the top of the spaceship.
    ld a, (player_y + 1)
    ld e, a
    call generic_getY
    inc hl
    inc hl
    
    ; Load the value at the top of the spaceship into a. Subtract the value that should be there,
    ; and return if this is non-zero: I.e we have a collision.
    ld a, (hl)
    sub d
    ret nz
    
    ; Now load into hl the screen memory location of the bottom of the spaceship.
    ld a, e
    add 8
    ld e, a
    call generic_getY
    inc hl
    inc hl
    
    ; Load the value at the top of the spaceship into a. Subtract the value that should be there,
    ; here we end anyway.
    ld a, (hl)
    sub d
ret






; ------------------------- SPEED METHODS ----------------------------


; Runs a y position step. This adds the yspeed to the player's y position, and redraws the player
; if the player's real y position has changed.
;
; (simple)
;
; Parameters:
;       NONE.
; Modifies:
;       a, bc, de, hl
; Returns:
;       NONE
;
player_yPosStep:
    
    ; Load the player y, preserve the old position of the player's real y position into c.
    ld hl, (player_y)
    ld c, h
    
    ; Add player yspeed onto hl (the player's y), then store back into player_y.
    ld de, (player_ySpeed)
    add hl, de
    ld (player_y), hl
    
    ; Now return and do nothing if the old position (stored in c) is the same as the new position.
    ld a, c
    sub h
    ret z
    
    ; Now zero the player area and draw area (if we have moved).
    ld b, h
    ld e, c
    call player_zeroPlayerArea
    ld e, b
    call player_drawPlayer
ret


; ------------------------- PLAYER DRAWING METHODS -------------------


; Zeroes the player's draw area. This must be called before redrawing the player.
;
; (simple)
;
; Parameters:
;       NONE.
; Modifies:
;       a, e, hl
; Returns:
;       NONE
;
player_zeroPlayerArea:
    call generic_getY
    inc hl
    inc hl
    
    ld (hl), 0
    call generic_incrementY
    ld (hl), 0
    call generic_incrementY
    ld (hl), 0
    call generic_incrementY
    ld (hl), 0
    call generic_incrementY
    ld (hl), 0
    call generic_incrementY
    ld (hl), 0
    call generic_incrementY
    ld (hl), 0
    call generic_incrementY
    ld (hl), 0
    call generic_incrementY
    ld (hl), 0
ret

; Draws the player to the screen.
;
; (simple)
;
; Parameters:
;       NONE.
; Modifies:
;       a, e, hl
; Returns:
;       NONE
;
player_drawPlayer:
    call generic_getY
    inc hl
    inc hl
    
    ld e, 00111111b ; Load e with this value for convenience
    
    ld a, (hl)
    xor e       ; For top and bottom bytes, XOR with the value that is already there.
    ld (hl), a ; This way the collision detection detects if we moved vertically into a wall.
    
    call generic_incrementY
    ld (hl), 01111000b
    call generic_incrementY
    ld (hl), 11110000b
    call generic_incrementY
    ld (hl), 11111110b
    call generic_incrementY
    ld (hl), 00111111b
    call generic_incrementY
    ld (hl), 11111110b
    call generic_incrementY
    ld (hl), 11110000b
    call generic_incrementY
    ld (hl), 01111000b
    call generic_incrementY
    
    ld a, (hl)
    xor e       ; For top and bottom bytes, XOR with the value that is already there.
    ld (hl), a ; This way the collision detection detects if we moved vertically into a wall.
    
ret











; ------------------------------- PLAYER BLINK ANIMATION FOR END OF GAME -------------------------







player_blink:
    
    ld d, 16
    player_blinkLoop:
        call player_runOneBlinkCycle
        ld e, 6
        call generic_waitForFrames
        dec d
    jp nz, player_blinkLoop
ret



player_runOneBlinkCycle:
    ld a, (player_y + 1)
    ld e, a
    call generic_getY
    inc hl
    inc hl
    
    ld a, (hl)
    xor 00111111b
    ld (hl), a
    call generic_incrementY
    ld a, (hl)
    xor 01111000b
    ld (hl), a
    call generic_incrementY
    ld a, (hl)
    xor 11110000b
    ld (hl), a
    call generic_incrementY
    ld a, (hl)
    xor 11111110b
    ld (hl), a
    call generic_incrementY
    ld a, (hl)
    xor 00111111b
    ld (hl), a
    call generic_incrementY
    ld a, (hl)
    xor 11111110b
    ld (hl), a
    call generic_incrementY
    ld a, (hl)
    xor 11110000b
    ld (hl), a
    call generic_incrementY
    ld a, (hl)
    xor 01111000b
    ld (hl), a
    call generic_incrementY
    ld a, (hl)
    xor 00111111b
    ld (hl), a
    
ret





