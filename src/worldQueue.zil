




; Queues contain maximum of 512 items.
; Each item is 2 bytes
; The length (in bytes) of each of the 2 queues is 1024.
; Queues are aligned to 1024 bytes,
; so the address of each queue is:    xxxx xxNN NNNN NNNN
; ... where N is the position within the queue
; So to wrap to the beginning of the queue, we reset the first 6 bits of the largest byte.


QUEUE_SPACE: defs 1024 + 1024*2

QUEUE0: equ ((QUEUE_SPACE + 1024 + 0000)/1024)*1024
QUEUE1: equ ((QUEUE_SPACE + 1024 + 1024)/1024)*1024

QUEUE0_STARTPOS: defw QUEUE0
QUEUE1_STARTPOS: defw QUEUE1
QUEUE0_ENDPOS: defw QUEUE0
QUEUE1_ENDPOS: defw QUEUE1


; Since the queues are aligned to the nearest 1024 bytes, it is the case that:
;   > For EVERY position within the queue, the 6 most significant bits of the memory address do not change
;   > The most significant 6 bits of the memory address are unique to THIS QUEUE ONLY.
; 
; We need some sort of method of wrapping memory addresses back to the beginning of the queue, when we
; iterate to the end.
; Since the least-significant 10 bits of the memory address correspond to the position within the queue, we
; should allow the least significant 10 bits to wrap, while keeping the most significant 6 bits the same.
;
; HERE IS HOW THE FOLLOWING MACROS FOR WRAPPING WORK:
;
; > If
;   bit 10 of the original memory location is zero,
;   this can be achieved by
;   setting bit 10 to 0.
;   That way, wrapping is done like this:
;      - xxxxx01111111111   --> Increment -->   xxxxx10000000000   --> Set to zero -->   xxxxx00000000000
;             ^                                      ^                                        ^
; > If
;   bit 11 of the original memory location is zero,
;   bit 10 of the original memory location is one,
;   this can be achieved by
;   setting bit 11 to 0,
;   setting bit 10 to 1.
;   That way, wrapping is done like this:
;      - xxxx011111111111   --> Increment -->   xxxx100000000000   --> Set to zero -->   xxxx010000000000
;            ^^                                     ^^                                       ^^
; > If
;   bit 12 of the original memory location is zero,
;   bit 11 of the original memory location is one,
;   bit 10 of the original memory location is one,
;   this can be achieved by
;   setting bit 12 to 0,
;   setting bit 11 to 1,
;   setting bit 10 to 1.
;   That way, wrapping is done like this:
;      - xxx0111111111111   --> Increment -->   xxx1000000000000   --> Set to zero -->   xxx0110000000000
;           ^^^                                    ^^^                                      ^^^
;
;  ... and so on for bits 13,14,15.
;      The decision of which bits to set is implemented by the following macros, for queue 0 and 1.
;


if ((QUEUE0 / 256) & 00000100b) > 0
    if ((QUEUE0 / 256) & 00001000b) > 0
        if ((QUEUE0 / 256) & 00010000b) > 0
            if ((QUEUE0 / 256) & 00100000b) > 0
                if ((QUEUE0 / 256) & 01000000b) > 0
                    if ((QUEUE0 / 256) & 10000000b) > 0
                        QUEUE0_WRAPREG: macro reg
                            set 7, reg
                            set 6, reg
                            set 5, reg
                            set 4, reg
                            set 3, reg
                            set 2, reg
                        endm
                    else
                        QUEUE0_WRAPREG: macro reg
                            res 7, reg
                            set 6, reg
                            set 5, reg
                            set 4, reg
                            set 3, reg
                            set 2, reg
                        endm
                    endif
                else
                    QUEUE0_WRAPREG: macro reg
                        res 6, reg
                        set 5, reg
                        set 4, reg
                        set 3, reg
                        set 2, reg
                    endm
                endif
            else
                QUEUE0_WRAPREG: macro reg
                    res 5, reg
                    set 4, reg
                    set 3, reg
                    set 2, reg
                endm
            endif
        else
            QUEUE0_WRAPREG: macro reg
                res 4, reg
                set 3, reg
                set 2, reg
            endm
        endif
    else
        QUEUE0_WRAPREG: macro reg
            res 3, reg
            set 2, reg
        endm
    endif
else
    QUEUE0_WRAPREG: macro reg
        res 2, reg
    endm
endif




if ((QUEUE1 / 256) & 00000100b) > 0
    if ((QUEUE1 / 256) & 00001000b) > 0
        if ((QUEUE1 / 256) & 00010000b) > 0
            if ((QUEUE1 / 256) & 00100000b) > 0
                if ((QUEUE1 / 256) & 01000000b) > 0
                    if ((QUEUE1 / 256) & 10000000b) > 0
                        QUEUE1_WRAPREG: macro reg
                            set 7, reg
                            set 6, reg
                            set 5, reg
                            set 4, reg
                            set 3, reg
                            set 2, reg
                        endm
                    else
                        QUEUE1_WRAPREG: macro reg
                            res 7, reg
                            set 6, reg
                            set 5, reg
                            set 4, reg
                            set 3, reg
                            set 2, reg
                        endm
                    endif
                else
                    QUEUE1_WRAPREG: macro reg
                        res 6, reg
                        set 5, reg
                        set 4, reg
                        set 3, reg
                        set 2, reg
                    endm
                endif
            else
                QUEUE1_WRAPREG: macro reg
                    res 5, reg
                    set 4, reg
                    set 3, reg
                    set 2, reg
                endm
            endif
        else
            QUEUE1_WRAPREG: macro reg
                res 4, reg
                set 3, reg
                set 2, reg
            endm
        endif
    else
        QUEUE1_WRAPREG: macro reg
            res 3, reg
            set 2, reg
        endm
    endif
else
    QUEUE1_WRAPREG: macro reg
        res 2, reg
    endm
endif



; Adds an item to queue 0
;
; (simple)
;
; Parameters:
;       de: The memory location to add.
; Modifies:
;       hl
; Returns:
;       NONE
;
queue0_addItem:
    ld hl, (QUEUE0_ENDPOS)
    
    ld (hl), e
    inc hl
    ld (hl), d
    inc hl
    
    QUEUE0_WRAPREG h
    
    ld (QUEUE0_ENDPOS), hl
ret

; Adds an item to queue 1
;
; (simple)
;
; Parameters:
;       de: The memory location to add.
; Modifies:
;       hl
; Returns:
;       NONE
;
queue1_addItem:
    ld hl, (QUEUE1_ENDPOS)
    
    ld (hl), e
    inc hl
    ld (hl), d
    inc hl
    
    QUEUE1_WRAPREG h
    
    ld (QUEUE1_ENDPOS), hl
ret

; Removes an item from queue 0
;
; (simple)
;
; Parameters:
;       NONE
; Modifies:
;       hl
; Returns:
;       NONE
;
queue0_removeItem:
    ld hl, (QUEUE0_STARTPOS)
    inc hl
    inc hl
    QUEUE0_WRAPREG h
    ld (QUEUE0_STARTPOS), hl
ret

; Removes an item from queue 1
;
; (simple)
;
; Parameters:
;       NONE
; Modifies:
;       hl
; Returns:
;       NONE
;
queue1_removeItem:
    ld hl, (QUEUE1_STARTPOS)
    inc hl
    inc hl
    QUEUE1_WRAPREG h
    ld (QUEUE1_STARTPOS), hl
ret





; Updates a structure
;
; (simple)
;
; Parameters:
;       NONE
; Modifies:
;       a, hl, bc, de, a'
; Returns:
;       NONE
;
queue0_step:
    
    ld hl, (QUEUE0_STARTPOS)
    ld bc, (QUEUE0_ENDPOS)
    
    ex af, af'
    xor a        ; Zero the secondary accumulator
    ex af, af'
    
    
    jp queue0_stepLoopStart
    queue0_stepLoopStartReLoop:
        inc hl
        QUEUE0_WRAPREG h
    queue0_stepLoopStart:
        
        ; End the loop when hl reaches the same value as bc.
        ld a, l
        sub c
        jp nz, queue0_stepLoopContinue
        ld a, h
        sub b
        jp z, queue0_stepLoopEnd
        queue0_stepLoopContinue:
        
        ;ld a, r                             ; | FOR TESTING
        ;ld (SCREEN_MEMSTART + 8), a         ; | AND DEBUG
        
        ld e, (hl)
        inc hl       ; Load screen memory address pointed at by hl into de
        ld d, (hl)
        
        ld a, (de)
        sla a          ; Load byte from (de), shift left, store back.
        ld (de), a
        jp nz, queue0_stepLoopStartReLoop
        
        
        ld a, e
        and 31  ; Get the least significant 5 bits of the address
        
        jp nz, queue0_stepNormalDec
            
            ; If the first 5 bits are zero: (we have reached the left of the screen)
            ex af, af'
            inc a           ; Use the secondary accumulator to count the number of things to remove at the end.
            ex af, af'
            
        jp queue0_stepLoopStartReLoop
        queue0_stepNormalDec:
        
            ; If the first 5 bytes are not zero (we have just reached a byte boundary)
            
            ; So decrement the memory address
            dec e
            ; And store in the previous place. (Better way to do this part?)
            dec hl
            ld (hl), e
            inc hl
        
        jp queue0_stepLoopStartReLoop
    queue0_stepLoopEnd:

    
    ; Now to finish, we must remove from the queue any items that have got to the end.
    ; The number of them to remove has been counted in the secondary accumulator.
    ex af, af'
        and a ; Do a dummy instruction to set our flags from the value stored in the accumulator.
        queue0_stepRemoveLoop:
        jp z, queue0_stepRemoveLoopEnd
            
            call queue0_removeItem
            dec a
            
        jp queue0_stepRemoveLoop
        queue0_stepRemoveLoopEnd:
    
    ex af, af'
    
ret





; Updates a structure
; This method is almost identical to queue0_step. This should be done with a macro, but for some reason
; z80asm cannot seem to cope with labels within macros.
; Any changes to queue0_step should be duplicated for queue1_step.
; Additions from queue0_step are labelled with comments:  <<==
;
; (simple)
;
; Parameters:
;       NONE
; Modifies:
;       a, hl, bc, de, a'
; Returns:
;       NONE
;
queue1_step:
    
    ld hl, (QUEUE1_STARTPOS)
    ld bc, (QUEUE1_ENDPOS)
    
    ex af, af'
    xor a        ; Zero the secondary accumulator
    ex af, af'
    
    
    jp queue1_stepLoopStart
    queue1_stepLoopStartReLoop:
        inc hl
        QUEUE1_WRAPREG h
    queue1_stepLoopStart:
        
        ; End the loop when hl reaches the same value as bc.
        ld a, l
        sub c
        jp nz, queue1_stepLoopContinue
        ld a, h
        sub b
        jp z, queue1_stepLoopEnd
        queue1_stepLoopContinue:
        
        ;ld a, r                             ; | FOR TESTING
        ;ld (SCREEN_MEMSTART + 256 + 8), a   ; | AND DEBUG
        
        ld e, (hl)
        inc hl       ; Load screen memory address pointed at by hl into de
        ld d, (hl)
        
        ld a, (de)
        sla a          ; Load byte from (de), shift left, store back.
        inc a        ; <<== SINCE THIS IS QUEUE1, INC AFTER SHIFT TO FILL WITH 1's
        ld (de), a
        xor -1       ; <<== SINCE THIS IS QUEUE1, INVERT VALUE BEFORE ZERO CHECKING: WE ARE CHECKING FOR 0xff HERE.
                     ; <<== Note: Here we use xor -1 instead of "cpl", to set the flags from the new value.
        jp nz, queue1_stepLoopStartReLoop
        
        
        ld a, e
        and 31  ; Get the least significant 5 bits of the address
        
        jp nz, queue1_stepNormalDec
            
            ; If the first 5 bits are zero: (we have reached the left of the screen)
            ex af, af'
            inc a           ; Use the secondary accumulator to count the number of things to remove at the end.
            ex af, af'
            
        jp queue1_stepLoopStartReLoop
        queue1_stepNormalDec:
        
            ; If the first 5 bytes are not zero (we have just reached a byte boundary)
            
            ; So decrement the memory address
            dec e
            ; And store in the previous place. (Better way to do this part?)
            dec hl
            ld (hl), e
            inc hl
        
        jp queue1_stepLoopStartReLoop
    queue1_stepLoopEnd:

    
    ; Now to finish, we must remove from the queue any items that have got to the end.
    ; The number of them to remove has been counted in the secondary accumulator.
    ex af, af'
        and a ; Do a dummy instruction to set our flags from the value stored in the accumulator.
        queue1_stepRemoveLoop:
        jp z, queue1_stepRemoveLoopEnd
            
            call queue1_removeItem
            dec a
            
        jp queue1_stepRemoveLoop
        queue1_stepRemoveLoopEnd:
    
    ex af, af'
    
ret







; Automatically does all the operations to add the location relating to the right
; side of the given line.
;
; (simple)
;
; Parameters:
;       e: The line on the screen to add, measured as zero=top.
; Modifies:
;       a, hl
; Returns:
;       NONE
;
queue0_addLine:
    call generic_getY
    push de
        ld d, h
        ld a, l
        add SCREEN_COLS - 1
        ld e, a
        call queue0_addItem
    pop de
ret


; Automatically does all the operations to add the location relating to the right
; side of the given line.
;
; (simple)
;
; Parameters:
;       e: The line on the screen to add, measured as zero=top.
; Modifies:
;       a, hl
; Returns:
;       NONE
;
queue1_addLine:
    call generic_getY
    push de
        ld d, h
        ld a, l
        add SCREEN_COLS - 1
        ld e, a
        call queue1_addItem
    pop de
ret






; Clears queue 0.
;
; (simple)
;
; Parameters:
;       NONE.
; Modifies:
;       hl
; Returns:
;       NONE
;
queue0_clear:
    ld hl, QUEUE0
    ld (QUEUE0_STARTPOS), hl
    ld (QUEUE0_ENDPOS), hl
ret





; Clears queue 1.
;
; (simple)
;
; Parameters:
;       NONE.
; Modifies:
;       hl
; Returns:
;       NONE
;
queue1_clear:
    ld hl, QUEUE1
    ld (QUEUE1_STARTPOS), hl
    ld (QUEUE1_ENDPOS), hl
ret
