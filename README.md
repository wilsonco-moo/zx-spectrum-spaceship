
Spaceship in a cave, for the Sinclair ZX Spectrum
=================================================

This is a 60 fps sidescrolling game, for the ZX Spectrum, written using Zilog Z80 assembly. I designed this 
in June 2018: this my second game project for the ZX Spectrum.

Provided is the source code for the game, in the src/ directory, and two files: spaceship.wav and spaceship.tap. The .tap file can be loaded
into any ZX Spectrum emulator, and the .wav file can be used to load the game into a real ZX Spectrum.

If you wish to compile the game yourself, read the following instructions.

## Before compiling

 * Install the [z80asm assembler](https://packages.debian.org/stretch/z80asm). This is available from the software repository
   of many linux distributions.
 * Download and compile [bin2tap](http://metalbrain.speccy.org/bin2tap.c). bin2tap can be compiled with
   gcc, for example: `gcc bin2tap.c -o bin2tap`. Move the bin2tap executable to /opt/bin2tap, as that is where the makefile expects it to be.
   If you want it to be placed somewhere else, the makefile can be modified.
 * Download and compile [zxtap-to-wav](https://github.com/raydac/zxtap-to-wav). The github page
   contains documentation about how to compile this. Again, move the zxtap-to-wav executable to /opt/zxtap-to-wav, as that is where the makefile expects it to be.
   If you want it to be placed somewhere else, the makefile can be modified.

## How to compile and convert to a wav file

 * To assemble the source code, cd to the src/ directory, and run: `make assemble`. This will produce the .tap file: src/compile/main.tap
 * To convert this into a wav file, to transfer to a real ZX spectrum, cd to the src/ directory, and run: `make noise`. This will produce
   the .wav file src/compile/main.wav.

**Acknowledgements:**
 * Andrew Ackerley, for the design of the menu/logo screen.

# Screenshots

The game's main menu/logo screen.

![Image](screenshots/menu.png)

A screenshot during a game of Spaceship in a cave.

![Image](screenshots/inGame.png)
